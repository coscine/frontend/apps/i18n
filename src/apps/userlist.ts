import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    userListHeadline: "Members",
    userManagementLinkText: "Manage Members...",
    leaveButton: "leave",
    leaveTitle1: "Leave project ",
    leaveTitle2: "",
    leave1: "You are about to leave project ",
    leave2:
      ". You will also loose access to the project and its resources. Would you like to leave?",
    leave: "Leave",
    cancel: "Cancel",
    popover: "Leave the project",
    disabledPopover:
      "You are the last project owner, add another project owner to leave this project",
  },
  de: {
    userListHeadline: "Mitglieder",
    userManagementLinkText: "Mitglieder verwalten...",
    leaveButton: "Verlassen",
    leaveTitle1: "Projekt ",
    leaveTitle2: " verlassen",
    leave1: "Sie verlassen das Projekt ",
    leave2:
      ". Damit verlieren Sie Zugriff auf das Projekt und die Ressourcen des Projekts. Möchten Sie das Projekt verlassen?",
    leave: "Verlassen",
    cancel: "Abbrechen",
    popover: "Das Projekt verlassen",
    disabledPopover:
      "Sie sind der einzige Projekt Owner. Fügen Sie einen weiteren Projekt Owner hinzu, um das Projekt verlassen zu können.",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
