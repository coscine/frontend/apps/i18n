import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    back: "Back",
    createProjectMenuText: "Create a project",
    displayName: "Display Name",
    displayNameLabel: "Display Name:",
    displayNameHelp:
      "This is a required field and can only be up to 25 characters long.",
    projectName: "Project Name",
    projectNameLabel: "Project Name:",
    projectNameHelp:
      "This is a required field and can only be up to 200 characters long.",
    projectDescription: "Project Description",
    projectDescriptionLabel: "Project Description:",
    projectDescriptionHelp:
      "This is a required field and can only be up to 5000 characters long.",
    projectStart: "Project Start",
    projectStartLabel: "Project Start:",
    projectEnd: "Project End",
    projectEndLabel: "Project End:",
    projectKeywordsLabel: "Project Keywords:",
    projectKeywords: 'Type then press "enter" to insert a Keyword.',
    projectKeywordsHelp:
      "This is a required field and can only be up to 1000 characters.",
    deleteNameHelp: "Please enter the name of the project to be deleted.",
    archive: "Archive",
    deleteButton: "Delete",
    save: "Submit",
    cancel: "Cancel",
    clearFormButton: "Reset",

    projectPrincipleInvestigatorsLabel: "Principal Investigators (PIs):",
    projectPrincipleInvestigators: "Principal Investigators (PIs)",
    projectPrincipleInvestigatorsHelp:
      "This is a required field and can only be up to 500 characters long.",
    projectDisciplineLabel: "Discipline:",
    projectDiscipline: "Discipline",
    projectOrganizationLabel: "Participating Organizations:",
    projectOrganization: "Participating Organizations",
    projectOrganizationNoOptions: "Search for organizations",
    projectVisibilityLabel: "Visibility:",
    projectVisibility: "Visibility",
    projectGrantIdLabel: "Grant ID:",
    projectGrantId: "Grant ID",
    projectGrantIdHelp:
      "This is a required field and can only be up to 500 characters long.",

    deleteModalHeadline: "Do you really want to delete this project?",
    deleteModalDescription:
      "If you are sure you really want to delete this project, please repeat the project name:",
    deleteModalHelp: "The entered name does not match the project name.",

    tagPlaceholder: "You can add this tag",
    activatedFeaturesHeadline: "Project features",

    activatedImportFromParent: "Project Metadata",
    copyMetadataButton: "COPY METADATA",
    copyMetadataLabel: "Copy metadata from",

    affliliationNotification:
      "As a member of RWTH Aachen your project will be provided with 25 GB quota for RDS storage.",

    loadingSpinnerProjectCreation:
      "Creating projects currently takes up to a minute. Thank you for your patience.",
  },
  de: {
    back: "Zurück",
    createProjectMenuText: "Projekt erstellen",
    displayName: "Anzeigename",
    displayNameLabel: "Anzeigename:",
    displayNameHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 25 Zeichen.",
    projectName: "Projektname",
    projectNameLabel: "Projektname:",
    projectNameHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",
    projectDescription: "Projektbeschreibung",
    projectDescriptionLabel: "Projektbeschreibung:",
    projectDescriptionHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 5000 Zeichen.",
    projectStart: "Projektstart",
    projectStartLabel: "Projektstart:",
    projectEnd: "Projektende",
    projectEndLabel: "Projektende:",
    projectKeywordsLabel: "Projektschlagwörter:",
    projectKeywords:
      'Tippen Sie und drücken dann "Enter" um ein Schlagwort einzufügen.',
    projectKeywordsHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 1000 Zeichen.",
    deleteNameHelp:
      "Bitte geben Sie den Projektnamen des zu löschenden Projekts an.",
    archive: "Archivieren",
    deleteButton: "Entfernen",
    save: "Abschicken",
    cancel: "Abbrechen",
    clearFormButton: "Zurücksetzen",

    projectPrincipleInvestigatorsLabel: "Principal Investigators (PIs):",
    projectPrincipleInvestigators: "Principal Investigators (PIs)",
    projectPrincipleInvestigatorsHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 500 Zeichen.",
    projectDisciplineLabel: "Disziplin:",
    projectDiscipline: "Disziplin",
    projectOrganizationLabel: "Teilnehmende Organisation:",
    projectOrganization: "Teilnehmende Organisation",
    projectOrganizationNoOptions: "Suche nach Organisationen",
    projectVisibilityLabel: "Sichtbarkeit:",
    projectVisibility: "Sichtbarkeit",
    projectGrantIdLabel: "Grant ID:",
    projectGrantId: "Grant ID",
    projectGrantIdHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 500 Zeichen.",

    deleteModalHeadline: "Projekt wirklich entfernen?",
    deleteModalDescription:
      "Wenn Sie sicher sind, dass Sie dieses Projekt entfernen möchten, wiederholen Sie bitte den Projektnamen:",
    deleteModalHelp:
      "Der angegebene Name stimmt nicht mit dem Projektname überein.",

    tagPlaceholder: "Sie können diesen Tag hinzufügen",
    activatedFeaturesHeadline: "Projekteinstellungen",

    activatedImportFromParent: "Projekt Metadaten",
    copyMetadataButton: "Metadaten kopieren",
    copyMetadataLabel: "Kopiere Metadaten von",

    affliliationNotification:
      "Als Mitglied der RWTH Aachen wird Ihr Projekt mit einem Speicherplatz von 25 GB für RDS-Speicher ausgestattet.",

    loadingSpinnerProjectCreation:
      "Die Projekterstellung kann derzeit bis zu einer Minute dauern. Vielen Dank für Ihre Geduld.",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
