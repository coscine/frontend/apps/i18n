import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    addProject: "Add Project",
    subprojects: "Sub-Projects",
  },
  de: {
    addProject: "Projekt hinzufügen",
    subprojects: "Teilprojekte",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
