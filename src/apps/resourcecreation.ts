import Default from "./default";
import ResourceTypeDefinitions from "./resourcetypedefinitions";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    popoverLicense: "For more information on licenses see ",
    popoverUsageRights: "For more information on internal rules for reuse see ",
    help: "Help",
    
    popoverHeader: "Quota not sufficient",
    popoverContent:
      "This project does not have sufficient quota to create more resources. You can apply for more storage space in the quota management.",
    resourceCreationErrorHeader: "Failed to create resource",
    resourceCreationError:
      "Please try again. If the problem persists, please contact ",
    back: "Back",
    next: "Next",
    confirm: "Confirm",

    or: "<b>OR</b>",
    create: "Create",
    createModalTitle: "Creation of Application Profiles:",
    createModalBody:
      "<p>You are going to get redirected to another page to configure and create a customized application profile.</p> <p>The review process for provisioning submissions usually takes a few days. If required, you will be contacted by our consulting team to support you with the process. </p> <p> Do you want to get redirected to the application profile creation page?</p></br>",

    tooltipCreate: "Request for creation of application profiles",
    tooltipCreateDisabled:
      "Please add and verify your email address {linkToUserProfile} to request for creation of application profiles",
    here: "HERE",

    needMore: "Need more?",

    title: "Add Resource",

    resourceconfiguration: "Resource Configuration",
    generalinformation: "General Information",

    resourceTypesLabel: "Resource Type:",
    resourceTypes: "Resource Type",
    selectResourceType: "Please select a resource type",

    BucketName: "Bucket Name",
    BucketNameLabel: "Bucket Name:",
    BucketNameHelp:
      "This is a required field and can only be from 3 - 63 characters long.",

    Size: "Resource Size",
    SizeLabel: "Resource Size:",
    SizeHelp: "This is a required field.",

    AccessKey: "Access Key",
    AccessKeyLabel: "Access Key:",
    AccessKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    ReadAccessKey: "Access Key (Reading)",
    ReadAccessKeyLabel: "Access Key (Reading):",
    ReadAccessKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    WriteAccessKey: "Access Key (Writing)",
    WriteAccessKeyLabel: "Access Key (Writing):",
    WriteAccessKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    SecretKey: "Secret Key",
    SecretKeyLabel: "Secret Key:",
    SecretKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    ReadSecretKey: "Secret Key (Reading)",
    ReadSecretKeyLabel: "Secret Key (Reading):",
    ReadSecretKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    WriteSecretKey: "Secret Key (Writing)",
    WriteSecretKeyLabel: "Secret Key (Writing):",
    WriteSecretKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    Endpoint: "Entry Point",
    EndpointLabel: "Entry Point:",
    EndpointHelp:
      "This is a required field and can only be up to 200 characters long.",

    ResourceUrl: "Resource Url",
    ResourceUrlLabel: "Resource Url:",
    ResourceUrlHelp:
      "This is a required field and can only be up to 200 characters long.",

    RepositoryNumber: "Repository Id",
    RepositoryNumberLabel: "Repository Id:",
    RepositoryNumberHelp: "This is a required field and can only be an number.",

    RepositoryUrl: "Repository Url",
    RepositoryUrlLabel: "Repository Url:",
    RepositoryUrlHelp:
      "This is a required field and can only be up to 500 characters long.",

    Token: "Token",
    TokenLabel: "Token:",
    TokenHelp:
      "This is a required field and can only be up to 100 characters long.",

    ResourceName: "Resource Name",
    ResourceNameLabel: "Resource Name:",
    ResourceNameHelp:
      "This is a required field and can only be up to 200 characters long.",

    DisplayName: "Display Name",
    DisplayNameLabel: "Display Name:",
    DisplayNameHelp:
      "This is a required field and can only be up to 25 characters long.",

    ResourceDescription: "Resource Description",
    ResourceDescriptionLabel: "Resource Description:",
    ResourceDescriptionHelp:
      "This is a required field and can only be up to 5000 characters long.",

    ResourceKeywords: 'Type then press "enter" to insert a Keyword.',
    ResourceKeywordsLabel: "Resource Keywords:",
    ResourceKeywordsHelp:
      "This is a required field and can only be up to 1000 characters.",

    ResourceDiscipline: "Discipline",
    ResourceDisciplineLabel: "Discipline:",

    ResourceVisibility: "Visibility",
    ResourceVisibilityLabel: "Visibility:",

    ResourceLicense: "License",
    ResourceLicenseLabel: "License:",
    SelectResourceLicense: "Please select a License",

    ResourceUsageRights: "Usage Rights",
    ResourceUsageRightsLabel: "Internal Rules for Reuse:",
    ResourceUsageRightsHelp:
      "This is a required field and can only be up to 500 characters long.",

    TagPlaceholder: "You can add this tag",

    metadata: "Resource Metadata",

    applicationProfilesLabel: "Application Profiles:",
    applicationProfiles: "Application Profiles",

    selectApplicationProfile: "Please select an application profile",

    CreatorLabel: "Creator:",
    Creator: "Creator",
    TitleLabel: "Title:",
    Title: "Title",
    ProductionDateLabel: "Production Date:",
    ProductionDate: "Production Date",
    SubjectAreaLabel: "Subject Area:",
    SubjectArea: "Subject Area",
    ResourceLabel: "Resource:",
    Resource: "Resource",
    RightsLabel: "Rights:",
    Rights: "Rights",
    RightsholderLabel: "Rightsholder:",
    Rightsholder: "Rightsholder",

    DeleteButton: "Delete",
    cancel: "Cancel",
    proceed: "Proceed",
    DeleteModalHeadline: "Do you really want to delete this resource?",
    DeleteModalDescription:
      "If you are sure you really want to delete this resource, please repeat the resource name:",
    DeleteModalHelp: "The entered name does not match the resource name.",

    stepSetup: "Step 1: Setup",
    stepGeneralInfo: "Step 2: General Information",
    stepMetadata: "Step 3: Resource Metadata",
    stepOverview: "Step 4: Overview & Confirm",

    validationErrorMessage:
      "No connection with the resource is possible. Please check the provided data.",

    PID: "Persistent ID:",

    ApplyForBucket: "Next",
    BucketSizeNoteMiddle: " GB, this equals approximately ",
    BucketSizeNoteEnd: " files.",

    MultiselectPlaceholderApplicationProfile:
      "Please select an application profile.",
    MultiselectPlaceholderResourceType: "Please select a resource type.",
    MultiselectSelectEnter: "Press enter to select.",
    MultiselectNoResults:
      "No elements found. Consider changing the search query.",
    MultiselectNoOptions: "List is empty.",
  },
  de: {
    popoverLicense: "Für weitere Informationen zu Lizenzen siehe ",
    popoverUsageRights: "Für weitere Informationen zu internen Regeln zur Nachnutzung siehe ",
    help: "Hilfe",
    
    popoverHeader: "Quota nicht ausreichend",
    popoverContent:
      "Dieses Projekt hat nicht genug Quota um weitere Ressourcen anlegen zu können. Im Quotamanagement können Sie zusätzlichen Speicher beantragen.",
    resourceCreationErrorHeader: "Fehler beim Erstellen der Ressource",
    resourceCreationError:
      "Bitte versuchen Sie es erneut. Falls der Fehler wieder Auftritt, wenden Sie sich bitte an ",
    back: "Zurück",
    next: "Weiter",
    confirm: "Bestätigen",

    or: "<b>ODER</b>",
    create: "Neu erstellen",
    createModalTitle: "Erstellung von Applikationsprofilen:",
    createModalBody:
      "<p>Zur Konfiguration und Erstellung eines individuellen Applikationsprofils werden Sie auf eine andere Webseite umgeleitet. </p> <p> Der Review-Prozess für die Bereitstellung des Profils dauert für gewöhnlich einige Tage. Falls nötig, werden Sie von unserem Consulting-Team kontaktiert, um Sie in diesem Prozess zu unterstützen. </p> <p> Möchten Sie zur Webseite zur Erstellung eines Applikationsprofils weiterleitet werden?</p></br>",

    tooltipCreate: "Anfrage zur Erstellung eines Applikationsprofils",
    tooltipCreateDisabled:
      "Fügen Sie bitte {linkToUserProfile} Ihre Emailadresse hinzu und verfizieren Sie diese, um eine Anfrage zur Erstellung von neuen Applikationsprofilen senden zu können",
    here: "hier",

    needMore: "Benötigen Sie mehr Speicher?",

    title: "Ressource hinzufügen",

    resourceconfiguration: "Ressourcen Konfiguration",
    generalinformation: "Generelle Informationen",

    resourceTypesLabel: "Ressourcentypen:",
    resourceTypes: "Ressourcentyp",
    selectResourceType: "Bitte wählen Sie einen Ressourcentypen aus",

    BucketName: "Ressourcenname",
    BucketNameLabel: "Ressourcenname:",
    BucketNameHelp:
      "Dieses Feld ist erforderlich und besitzt eine Minimallänge von 3 und eine Maximallänge von 63 Zeichen.",

    Size: "Speicher für Ressource",
    SizeLabel: "Speicher für Ressource:",
    SizeHelp: "Dieses Feld ist erforderlich.",

    AccessKey: "Access Key",
    AccessKeyLabel: "Access Key:",
    AccessKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    ReadAccessKey: "Access Key (Lesen)",
    ReadAccessKeyLabel: "Access Key (Lesen):",
    ReadAccessKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    WriteAccessKey: "Access Key (Schreiben)",
    WriteAccessKeyLabel: "Access Key (Schreiben):",
    WriteAccessKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    SecretKey: "Secret Key",
    SecretKeyLabel: "Secret Key:",
    SecretKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    ReadSecretKey: "Secret Key (Lesen)",
    ReadSecretKeyLabel: "Secret Key (Lesen):",
    ReadSecretKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    WriteSecretKey: "Secret Key (Schreiben)",
    WriteSecretKeyLabel: "Secret Key (Schreiben):",
    WriteSecretKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    Endpoint: "Ressourcen Url",
    EndpointLabel: "Ressourcen Url:",
    EndpointHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    ResourceUrl: "Ressourcen Url",
    ResourceUrlLabel: "Ressourcen Url:",
    ResourceUrlHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    RepositoryNumber: "Repository Id",
    RepositoryNumberLabel: "Repository Id:",
    RepositoryNumberHelp:
      "Dieses Feld ist erforderlich und kann nur eine Nummer sein.",

    RepositoryUrl: "Repository Url",
    RepositoryUrlLabel: "Repository Url:",
    RepositoryUrlHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 500 Zeichen.",

    Token: "Token",
    TokenLabel: "Token:",
    TokenHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 100 Zeichen.",

    ResourceName: "Ressourcenname",
    ResourceNameLabel: "Ressourcenname:",
    ResourceNameHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    DisplayName: "Anzeigename",
    DisplayNameLabel: "Anzeigename:",
    DisplayNameHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 25 Zeichen.",

    ResourceDescription: "Ressourcenbeschreibung",
    ResourceDescriptionLabel: "Ressourcenbeschreibung:",
    ResourceDescriptionHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 5000 Zeichen.",

    ResourceKeywords:
      'Tippen Sie und drücken dann "Enter" um ein Schlagwort einzufügen.',
    ResourceKeywordsLabel: "Ressourcenschlagwörter:",
    ResourceKeywordsHelp:
      "Dieses Feld ist optional und besitzt eine Maximallänge von 1000 Zeichen.",

    ResourceDiscipline: "Disziplin",
    ResourceDisciplineLabel: "Disziplin:",

    ResourceVisibility: "Sichtbarkeit",
    ResourceVisibilityLabel: "Sichtbarkeit:",

    ResourceLicense: "Lizenz",
    ResourceLicenseLabel: "Lizenz:",
    SelectResourceLicense: "Bitte wählen Sie eine Lizenz aus",

    ResourceUsageRights: "Verwendungsrechte",
    ResourceUsageRightsLabel: "Interne Regeln zur Nachnutzung:",
    ResourceUsageRightsHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 500 Zeichen.",

    TagPlaceholder: "Sie können diesen Tag hinzufügen",

    metadata: "Metadaten der Ressource",

    applicationProfilesLabel: "Applikationsprofile:",
    applicationProfiles: "Applikationsprofile",

    selectApplicationProfile: "Bitte wählen sie ein Applikationsprofil aus",

    CreatorLabel: "Ersteller:",
    Creator: "Ersteller",
    TitleLabel: "Titel:",
    Title: "Titel",
    ProductionDateLabel: "Erstelldatum:",
    ProductionDate: "Erstelldatum",
    SubjectAreaLabel: "Fachrichtung:",
    SubjectArea: "Fachrichtung",
    ResourceLabel: "Ressource:",
    Resource: "Ressource",
    RightsLabel: "Berechtigung:",
    Rights: "Berechtigung",
    RightsholderLabel: "Rechteinhaber:",
    Rightsholder: "Rechteinhaber",

    DeleteButton: "Löschen",
    cancel: "Abbrechen",
    proceed: "Fortsetzen",
    DeleteModalHeadline: "Ressource wirklich entfernen?",
    DeleteModalDescription:
      "Wenn Sie sicher sind, dass Sie diese Ressource entfernen möchten, wiederholen Sie bitte den Ressourcennamen:",
    DeleteModalHelp:
      "Der angegebene Name stimmt nicht mit dem Ressourcenname überein",

    stepSetup: "Schritt 1: Setup",
    stepGeneralInfo: "Schritt 2: Generelle Informationen",
    stepMetadata: "Schritt 3: Metadaten der Ressource",
    stepOverview: "Schritt 4: Übersicht & Bestätigung",

    validationErrorMessage:
      "Es konnte keine Verbindung zur Ressource hergestellt werden. Bitte überprüfen Sie die Eingaben.",

    PID: "Persistente ID:",

    ApplyForBucket: "Weiter",
    BucketSizeNoteMiddle: " GB, dies entspricht circa ",
    BucketSizeNoteEnd: " Dateien.",

    MultiselectPlaceholderApplicationProfile:
      "Bitte wählen Sie ein Applikationsprofil aus.",
    MultiselectPlaceholderResourceType:
      "Bitte wählen Sie einen Ressourcentyp aus.",
    MultiselectSelectEnter: "Zum Auswählen Enter drücken.",
    MultiselectNoResults:
      "Keine Ergebnisse verfügbar. Bitte passen Sie die Suchanfrage an.",
    MultiselectNoOptions: "Die Liste ist leer.",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge(MergeUtil.merge({}, ResourceTypeDefinitions), Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
