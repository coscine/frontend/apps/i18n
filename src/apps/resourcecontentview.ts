import Default from "./default";
import ResourceTypeDefinitions from "./resourcetypedefinitions";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    resources: "Resources",

    upload: "Upload Files",
    download: "Download",

    edit: "Edit Resource Configuration",
    info: "Information",

    canDropFile: "You can drop your file here!",

    loading: "Loading...",
    typeToSearch: "Type to search",
    clear: "Clear",
    perPage: "Per page",
    more: "More",
    fileName: "Name",
    lastModified: "Modified",
    actions: "Actions",

    resourceName: "Resource Name",
    displayName: "Display Name",
    description: "Resource Description",
    disciplines: "Discipline",
    keywords: "Resource Keywords",
    visibility: "Visibility",
    license: "License",
    usageRights: "Usage Rights",

    PID: "Persistent ID",

    occupiedBytes: "Occupied Storage Space",

    noData: "This resource contains no data.",
    emptyTableText: "This resource contains no files.",
    emptyFilterText: "No files found matching your request.",

    metadataHeadline: "Metadata:",

    metadataManagerTitleUpload: "Upload Data:",
    metadataManagerTitleEdit: "Edit:",
    metadataManagerSelectLabel: "Selected Entry(s):",
    metadataManagerBadFileName:
      "Invalid file name. The following characters are not permissible: /:?*<>|",

    metadataManagerBtnCancel: "Cancel",
    metadataManagerBtnUpload: "Upload",
    metadataManagerBtnSelectFiles: "Select Files",

    metadataManagerBtnDelete: "Delete",
    metadataManagerBtnDownload: "Download",
    metadataManagerBtnReuse: "Reuse",
    metadataManagerBtnMove: "Move",
    metadataManagerBtnPublish: "Publish",
    metadataManagerBtnArchive: "Archive",
    metadataManagerBtnUpdate: "Update",
    metadataManagerBtnSaving: "Saving...",

    infoFileType: "File Type",
    infoFileTypeFolder: "Folder",
    infoFileTypeFile: "File",
    infoFileName: "File name",
    infoFileAbsolutePath: "File Path",
    infoFileLastModified: "Last modified",
    infoFileCreated: "Created",
    infoFileSize: "File Size",
    infoFileContent: "Content",
    infoFileFiles: "Files",
    infoFileNoInformation: "No Information",
    errorLoadingResource: "The resource could not be loaded.",
    tableIsEmpty: "There are currently no entities for this resource.",

    metadataManager: "Metadata Manager",
    allFiles: "All Files",

    validationErrors: "Validation Errors",

    modalSaveDuplicateFilesHeader: "Replace or skip duplicate files",
    modalSaveDuplicateFilesBody:
      "One or more files with the same name already exist. Uploading will result in overwriting the following file(s):",
    modalSaveDuplicateFilesBtnCancel: "CANCEL UPLOAD",
    modalSaveDuplicateFilesBtnSkip: "SKIP DUPLICATE FILES",
    modalSaveDuplicateFilesBtnOverwrite: "OVERWRITE",

    modalDeleteFilesHeader: "Delete files and metadata",
    modalDeleteFilesBody:
      "Are you sure, you want to delete the following files and metadata:",
    modalDeleteFilesBtnCancel: "CANCEL",
    modalDeleteFilesBtnDelete: "DELETE",

    modalLeavingPageHeader: "Upload in progress",
    modalLeavingPageBodyTop: "These files are currently uploading:",
    modalLeavingPageBodyBottom: "Are you sure you want to cancel the process?",
    modalLeavingPageBtnStay: "STAY ON PAGE",
    modalLeavingPageBtnLeave: "LEAVE CURRENT PAGE",

    toastSavingSuccessfulTitle: "Saving file(s) successful",
    toastSavingSuccessfulBody: "Number of files saved: ",

    toastSavingFailedTitle: "Saving file(s) failed",
    toastSavingFailedBodyTop:
      "An error occured while saving the following files:",
    toastSavingFailedBodyBottom: "Please try again.",

    dataUrl: "Data URL",
    metadataKey: "Entry Name",
    size: "File Size",

    linked: {
      upload: "Save Entries",
      download: "Open",

      noData: "This resource contains no entries.",
      emptyTableText: "This resource contains no entries.",
      emptyFilterText: "No entries found matching your request.",

      metadataManagerTitleUpload: "Save Entry:",
      metadataManagerSelectLabel: "Selected Entry(s):",
      metadataManagerBadFileName:
        "Invalid entry name. The following characters are not permissible: /:?*<>|",

      metadataManagerBtnDownload: "Open",
      metadataManagerBtnUpload: "Save",
      metadataManagerBtnSelectFiles: "New Entry",

      infoFileType: "Entry Type",
      infoFileTypeFolder: "Folder",
      infoFileTypeFile: "Entry",
      infoFileName: "Entry name",
      infoFileAbsolutePath: "Entry Path",
      infoFileSize: "Entry Size",
      infoFileFiles: "Entries",
      errorLoadingResource: "The resource could not be loaded.",
      tableIsEmpty: "There are currently no entries for this resource.",

      allFiles: "All Entries",

      modalSaveDuplicateFilesHeader: "Replace or skip duplicate entries",
      modalSaveDuplicateFilesBody:
        "One or more entries with the same name already exist. Saving will result in overwriting the following entry(s):",
      modalSaveDuplicateFilesBtnCancel: "CANCEL SAVING",
      modalSaveDuplicateFilesBtnSkip: "SKIP DUPLICATE ENTRIES",

      modalDeleteFilesHeader: "Delete entries and metadata",
      modalDeleteFilesBody:
        "Are you sure, you want to delete the following entries and metadata:",

      modalLeavingPageHeader: "Saving in progress",
      modalLeavingPageBodyTop: "These entries are currently saving:",

      toastSavingSuccessfulTitle: "Saving entries(s) successful",
      toastSavingSuccessfulBody: "Number of entries saved: ",

      toastSavingFailedTitle: "Saving entry(s) failed",
      toastSavingFailedBodyTop:
        "An error occured while saving the following entries:",
    },
  },
  de: {
    resources: "Ressourcen",

    upload: "Dateien hochladen",
    download: "Herunterladen",

    edit: "Ressourcenkonfiguration bearbeiten",
    info: "Informationen",

    canDropFile: "Sie können Ihre Datei hier ablegen!",

    loading: "Laden...",
    typeToSearch: "Suchen...",
    clear: "Leeren",
    perPage: "Pro Seite",
    more: "Mehr",
    fileName: "Name",
    lastModified: "Geändert",
    actions: "Aktionen",

    resourceName: "Ressourcenname",
    displayName: "Anzeigename",
    description: "Ressourcenbeschreibung",
    disciplines: "Disziplin",
    keywords: "Ressourcenschlagwörter",
    visibility: "Sichtbarkeit",
    license: "Lizenz",
    usageRights: "Verwendungsrechte",

    PID: "Persistente ID",

    occupiedBytes: "Belegter Speicher",

    noData: "Diese Ressource enthält keine Daten",
    emptyTableText: "Diese Ressource enthält keine Dateien.",
    emptyFilterText:
      "Keine Dateien gefunden die mit Ihrer Anfrage übereinstimmen.",

    metadataHeadline: "Metadaten:",

    metadataManagerTitleUpload: "Daten hochladen:",
    metadataManagerTitleEdit: "Editieren:",
    metadataManagerSelectLabel: "Einträge auswählen:",
    metadataManagerBadFileName:
      "Ungültiger Dateiname. Folgende Zeichen sind nicht zulässig: /:?*<>|",

    metadataManagerBtnCancel: "Abbrechen",
    metadataManagerBtnUpload: "Hochladen",
    metadataManagerBtnSelectFiles: "Dateien auswählen",

    metadataManagerBtnDelete: "Löschen",
    metadataManagerBtnDownload: "Herunterladen",
    metadataManagerBtnReuse: "Wiederverwenden",
    metadataManagerBtnMove: "Verschieben",
    metadataManagerBtnPublish: "Veröffentlichen",
    metadataManagerBtnArchive: "Archivieren",
    metadataManagerBtnUpdate: "Aktualisieren",
    metadataManagerBtnSaving: "Speichern...",

    infoFileType: "Dateiart",
    infoFileTypeFolder: "Ordner",
    infoFileTypeFile: "Dateien",
    infoFileName: "Dateiname",
    infoFileAbsolutePath: "Dateipfad",
    infoFileLastModified: "Zuletzt geändert",
    infoFileCreated: "Erstellt",
    infoFileSize: "Dateigröße",
    infoFileContent: "Inhalt",
    infoFileFiles: "Dateien",
    infoFileNoInformation: "Keine Information",
    errorLoadingResource: "Die Ressource konnte nicht geladen werden.",
    tableIsEmpty:
      "Es existieren aktuell noch keine Einträge für diese Ressource.",

    metadataManager: "Metadaten Manager",
    allFiles: "Alle Dateien",

    validationErrors: "Validierungsfehler",

    modalSaveDuplicateFilesHeader: "Dateien ersetzen oder überspringen",
    modalSaveDuplicateFilesBody:
      "Es sind bereits Dateien mit dem gleichen Namen vorhanden. Diese Dateien werden überschrieben:",
    modalSaveDuplicateFilesBtnCancel: "HOCHLADEN ABBRECHEN",
    modalSaveDuplicateFilesBtnSkip: "DIESE DATEIEN ÜBERSPRINGEN",
    modalSaveDuplicateFilesBtnOverwrite: "ÜBERSCHREIBEN",

    modalDeleteFilesHeader: "Löschen von Dateien und Metadaten",
    modalDeleteFilesBody:
      "Sind Sie sicher, dass Sie die folgenen Dateien und Metadaten löschen wollen:",
    modalDeleteFilesBtnCancel: "ABBRECHEN",
    modalDeleteFilesBtnDelete: "LÖSCHEN",

    modalLeavingPageHeader: "Dateien werden hochgeladen",
    modalLeavingPageBodyTop: "Diese Dateien werden momentan hochgeladen:",
    modalLeavingPageBodyBottom:
      "Sind Sie sicher, dass Sie den den Prozess abbrechen wollen?",
    modalLeavingPageBtnStay: "AUF SEITE BLEIBEN",
    modalLeavingPageBtnLeave: "SEITE VERLASSEN",

    toastSavingSuccessfulTitle: "Speichern erfolgreich",
    toastSavingSuccessfulBody: "Zahl der erfolgreich gespeicherten Dateien: ",

    toastSavingFailedTitle: "Speichern fehlgeschlagen",
    toastSavingFailedBodyTop:
      "Es ist ein Fehler beim Speichern der folgenden Dateien aufgetreten:",
    toastSavingFailedBodyBottom: "Bitte versuchen Sie es erneut.",

    dataUrl: "Daten URL",
    metadataKey: "Eintragsname",
    size: "Dateigröße",

    linked: {
      upload: "Einträge speichern",
      download: "Öffnen",

      noData: "Diese Ressource enthält keine Einträge",
      emptyTableText: "Diese Ressource enthält keine Einträge.",
      emptyFilterText:
        "Keine Einträge gefunden die mit Ihrer Anfrage übereinstimmen.",

      metadataManagerTitleUpload: "Einträge speichern:",
      metadataManagerSelectLabel: "Einträge auswählen:",
      metadataManagerBadFileName:
        "Ungültiger Eintrag. Folgende Zeichen sind nicht zulässig: /:?*<>|",

      metadataManagerBtnDownload: "Öffnen",
      metadataManagerBtnUpload: "Speichern",
      metadataManagerBtnSelectFiles: "Neuer Eintrag",

      infoFileType: "Eintrag",
      infoFileTypeFolder: "Ordner",
      infoFileTypeFile: "Einträge",
      infoFileName: "Eintragname",
      infoFileAbsolutePath: "Eintragpfad",
      infoFileSize: "Eintraggröße",
      infoFileFiles: "Einträge",
      errorLoadingResource: "Die Ressource konnte nicht geladen werden.",
      tableIsEmpty:
        "Es existieren aktuell noch keine Einträge für diese Ressource.",

      allFiles: "Alle Einträge",

      modalSaveDuplicateFilesHeader: "Einträge ersetzen oder überspringen",
      modalSaveDuplicateFilesBody:
        "Es sind bereits Einträge mit dem gleichen Namen vorhanden. Diese Einträge werden überschrieben:",
      modalSaveDuplicateFilesBtnCancel: "SPEICHERN ABBRECHEN",
      modalSaveDuplicateFilesBtnSkip: "DIESE EINTRÄGE ÜBERSPRINGEN",

      modalDeleteFilesHeader: "Löschen von Einträgen und Metadaten",
      modalDeleteFilesBody:
        "Sind Sie sicher, dass Sie die folgenen Einträge und Metadaten löschen wollen:",

      modalLeavingPageHeader: "Einträge werden hochgeladen",
      modalLeavingPageBodyTop: "Diese Einträge werden momentan hochgeladen:",

      toastSavingSuccessfulTitle: "Speichern erfolgreich",
      toastSavingSuccessfulBody:
        "Zahl der erfolgreich gespeicherten Einträge: ",

      toastSavingFailedTitle: "Speichern fehlgeschlagen",
      toastSavingFailedBodyTop:
        "Es ist ein Fehler beim Speichern der folgenden Einträge aufgetreten:",
    },
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge(MergeUtil.merge({}, ResourceTypeDefinitions), Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
