export default {
  en: {
    ResourceTypes: {
      rdsrwth: {
        displayName: "RWTH-RDS-Web",
        fullName: "Research Data Storage (RDS)",
        description:
          "Research Data Storage (RDS) is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
      },
      rdsude: {
        displayName: "UDE-RDS-Web",
        fullName: "Research Data Storage (RDS)",
        description:
          "Research Data Storage (RDS) University of Duisburg-Essen is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
      },
      rdstudo: {
        displayName: "TUDO-RDS-Web",
        fullName: "Research Data Storage (RDS)",
        description:
          "Research Data Storage (RDS) TU Dortmund University is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
      },
      rdsnrw: {
        displayName: "NRW-RDS-Web",
        fullName: "Research Data Storage (RDS)",
        description:
          "Research Data Storage (RDS) North Rhine-Westphalia is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
      },
      s3: {
        displayName: "S3 Bucket",
        fullName: "S3 Bucket (S3)",
        description:
          "S3 Buckets (S3) are object-based storage units for research data They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, a separate application procedure and a data management plan (DMP) is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
      },
      rdss3rwth: {
        displayName: "RWTH-RDS-S3",
        fullName: "RDS-S3-Resource (S3)",
        description:
          "RDS-S3 are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
      },
      rdss3ude: {
        displayName: "UDE-RDS-S3",
        fullName: "RDS-S3-Resource (S3)",
        description:
          "RDS-S3 University of Duisburg-Essen are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
      },
      rdss3tudo: {
        displayName: "TUDO-RDS-S3",
        fullName: "RDS-S3-Resource (S3)",
        description:
          "RDS-S3 TU Dortmund University are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
      },
      rdss3nrw: {
        displayName: "NRW-RDS-S3",
        fullName: "RDS-S3-Resource (S3)",
        description:
          "RDS-S3 North Rhine-Westphalia are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
      },
      gitlab: {
        displayName: "Gitlab",
        fullName: "Gitlab (Git)",
        description:
          "Gitlab (Git) is a web application for version management of software projects based on Git. You can store Git repositories in your CoScInE projects and describe them with metadata. In your daily work you can use Git as usual and have an overview of different repositories and the possibility to manage metadata in CoScInE.",
      },
      linked: {
        displayName: "Linked Data",
        fullName: "Linked Data (Linked)",
        description:
          "Linked Data Resources allow metadata management for data stored in external storage systems that cannot or should not be directly accessed via Coscine. For example, data on local data storage systems in the institute or external data storage systems that are not compatible with Coscine can be described with metadata. The handling of metadata is largely analogous to other resource types. Instead of uploading and downloading data, placeholders are created which are described with the metadata. An additional field can be used to specify a reference to the location of the file in order to allow a unique assignment at file level, e.g. Institute file server/Project X/Measurement B/11-12-2020_data.csv.",
      },
      rdss3wormrwth: {
        displayName: "RWTH-RDS-WORM",
        fullName: "RDS-WORM-Resource (WORM)",
        description:
          "RDS-WORM are object-based storage units with the function \"write once, read many (WORM)\" for research data. This resource type is especially suitable for raw data or other data that may no longer be changed. Attention! Once allocated storage space is no longer reusable, not even by the project owner.  RDS-WORM is based on the same technology as RDS-S3, so the management of metadata is up to the user. Metadata that should no longer be editable must be stored directly in the S3 bucket via additional files (e.g. README files). Metadata that is maintained via the Coscine web interface remains editable. An application process is required to ensure that the data stored in S3 buckets is described with metadata so that it can be retrieved and reused in the long term. In addition, it must be ensured that the data and metadata within the WORM S3-Buckets is stored with the awareness that it cannot be changed afterwards.",
      },
    },
  },
  de: {
    ResourceTypes: {
      rdsrwth: {
        displayName: "RWTH-FDS-Web",
        fullName: "Forschungsdatenspeicher (FDS)",
        description:
          "Forschungsdatenspeicher (FDS) ist objektbasierter Speicher für Forschungsdaten. Sie können FDS-Resourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzer können zusätzliche Speicherquota beantragen.",
      },
      rdsude: {
        displayName: "UDE-FDS-Web",
        fullName: "Forschungsdatenspeicher (FDS)",
        description:
          "Forschungsdatenspeicher (FDS) Universität Duisburg-Essen ist objektbasierter Speicher für Forschungsdaten. Sie können FDS-Resourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzer können zusätzliche Speicherquota beantragen.",
      },
      rdstudo: {
        displayName: "TUDO-FDS-Web",
        fullName: "Forschungsdatenspeicher (FDS)",
        description:
          "Forschungsdatenspeicher (FDS) Technische Universität Dortmund ist objektbasierter Speicher für Forschungsdaten. Sie können FDS-Resourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzer können zusätzliche Speicherquota beantragen.",
      },
      rdsnrw: {
        displayName: "NRW-FDS-Web",
        fullName: "Forschungsdatenspeicher (FDS)",
        description:
          "Forschungsdatenspeicher (FDS) Nordrhein-Westfalen ist objektbasierter Speicher für Forschungsdaten. Sie können FDS-Resourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzer können zusätzliche Speicherquota beantragen.",
      },
      s3: {
        displayName: "S3 Bucket",
        fullName: "S3 Bucket (S3)",
        description:
          "S3 Buckets (S3) sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Forschungsdatenspeicher (FDS), die Verwaltung der Metadaten liegt jedoch beim Nutzer. Daher ist ein gesondertes Antragsverfahren und ein Datenmanagementplan (DMP) notwendig, um sicherzustellen, dass die Daten die in S3 Buckets gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
      },
      rdss3rwth: {
        displayName: "RWTH-FDS-S3",
        fullName: "FDS-S3-Ressource (S3)",
        description:
          "FDS-S3 sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Forschungsdatenspeicher (FDS), die Verwaltung der Metadaten liegt jedoch beim Nutzer. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in FDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
      },
      rdss3ude: {
        displayName: "UDE-FDS-S3",
        fullName: "FDS-S3-Ressource (S3)",
        description:
          "FDS-S3 Universität Duisburg-Essen sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Forschungsdatenspeicher (FDS), die Verwaltung der Metadaten liegt jedoch beim Nutzer. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in FDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
      },
      rdss3tudo: {
        displayName: "TUDO-FDS-S3",
        fullName: "FDS-S3-Ressource (S3)",
        description:
          "FDS-S3 Technische Universität Dortmund sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Forschungsdatenspeicher (FDS), die Verwaltung der Metadaten liegt jedoch beim Nutzer. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in FDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
      },
      rdss3nrw: {
        displayName: "NRW-FDS-S3",
        fullName: "FDS-S3-Ressource (S3)",
        description:
          "FDS-S3 Nordrhein-Westfalen sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Forschungsdatenspeicher (FDS), die Verwaltung der Metadaten liegt jedoch beim Nutzer. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in FDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
      },
      gitlab: {
        displayName: "Gitlab",
        fullName: "Gitlab (Git)",
        description:
          "Gitlab (Git) ist eine Webanwendung zur Versionsverwaltung für Softwareprojekte auf Git-Basis. Sie können Git-Repositorien in Ihren CoScInE Projekten hinterlegen und mit Metadaten beschreiben. Im Arbeitsalltag können Sie Git wie gewohnt nutzen und haben in CoScInE die Übersicht über verschiedene Repositorien und die Möglichkeit zur  Metadatenverwaltung.",
      },
      linked: {
        displayName: "Linked Data",
        fullName: "Linked Data (Linked)",
        description:
          "Linked Data Ressourcen erlauben das Metadatenmanagemnent für Daten die in externen Speichersystemen liegen auf die nicht direkt per Coscine zugegriffen werden kann oder soll. So können z.B. Daten auf lokalen Datenspeichern im Institut oder externen Datenspeichern die nicht mit Coscine kompatibel sind mit Metadaten beschreiben werden. Die Handhabung der Metadaten erfolgt weitgehend analog zu anderen Ressourcentypen, statt dem Upload- und Download von Daten werden Platzhalter angelegt die mit den Metadaten beschrieben werden. Durch ein zusätzliches Feld kann eine Referenz auf den Ort der Datei angegeben werden um eine eindeutige Zuordnung auf Dateilevel zu erlauben, z. B. Institutsfileserver/Projekt X/Messreihe B/11-12-2020_data.csv.",
      },
      rdss3wormrwth: {
        displayName: "RWTH-RDS-WORM",
        fullName: "RDS-WORM-Ressource (WORM)",
        description:
          "RDS-WORM sind objektbasierte Speichereinheiten mit der Funktion \"write once, read many (WORM)\" für Forschungsdaten. Dieser Ressourcentyp ist besonders geeignet für Rohdaten oder andere Daten, die nicht mehr verändert werden dürfen. Achtung! Einmal zugewiesener Speicherplatz ist nicht mehr wiederverwendbar, auch nicht durch den Projekt Owner. RDS-WORM basiert auf der gleichen Technologie wie RDS-S3, so dass die Verwaltung der Metadaten dem Nutzer überlassen bleibt. Metadaten, die wie die zugehörigen Daten nicht mehr veränderbar sein sollen, müssen über zusätzliche Dateien (z. B. README-Dateien) direkt im S3-Bucket gespeichert werden. Metadaten, die über die Coscine-Weboberfläche gepflegt werden, bleiben editierbar. Es ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in RDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein. Darüber hinaus muss sichergestellt werden, dass die Daten und Metadaten innerhalb des WORM S3-Buckets in dem Bewusstsein abgespeichert werden, dass sie nachträglich nicht mehr verändert werden können.",
      },
    },
  },
};
