import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    breadCrumb: {
      createResource: "Add Resource",
      editResource: "Edit Resource",
      createProject: "Create Project",
      editProject: "Edit Project",
    },
    footer: {
      help: "Help",
      datenschutzLink: "Disclaimer",
      datenschutzLinkHref:
        "https://www.rwth-aachen.de/cms/root/Footer/Services/~cesv/Disclaimer/?lidx=1",
      imprint: "Site Credits",
      contact: "Contact",
    },
    profile: "Profile",
    preferences: "Preferences",
    search: "Search",
    langBox: "De",
    bannerText:
      'Coscine is currently in the pilot phase. You are welcome to explore the system, but you may experience limitations in availability and usage! If you have feedback you are welcome to send it to [ <a href="mailto:servicedesk@rwth-aachen.de?subject=Coscine%20Pilot%20Program">Servicedesk</a> ].',
    title: {
      project: {
        "CreateProject.aspx": "Create Project - {ProjectName} - Coscine",
        "CreateResource.aspx": "Create Resource - {ProjectName} - Coscine",
        "EditProject.aspx": "Edit Project - {ProjectName} - Coscine",
        "Home.aspx": "{ProjectName} - Coscine",
        "QuotaManagement.aspx": "Manage Quota - {ProjectName} - Coscine",
        "UserManagement.aspx": "Manage Users - {ProjectName} - Coscine",
        forms: {
          "Shared%20Documents": "Document Library - {ProjectName} - Coscine",
        },
        lists: {
          "Announcement%20Board":
            "Announcement Board - {ProjectName} - Coscine",
          "Discussion%20Board": "Discussion Board - {ProjectName} - Coscine",
        },
        resource: {
          "CreateResource.aspx":
            "Edit Resource - {ResourceName} - {ProjectName} - Coscine",
          "ViewResource.aspx":
            "View Resource - {ResourceName} - {ProjectName} - Coscine",
        },
      },
      "CreateProject.aspx": "Create Project - Coscine",
      "Home.aspx": "Home - Coscine",
      "Search.aspx": "Search - Coscine",
      "UserProfile.aspx": "User Profile - Coscine",
    },
  },
  de: {
    breadCrumb: {
      createResource: "Ressource hinzufügen",
      editResource: "Ressource bearbeiten",
      createProject: "Projekt erstellen",
      editProject: "Projekt bearbeiten",
    },
    footer: {
      help: "Hilfe",
      datenschutzLink: "Datenschutzerklärung",
      datenschutzLinkHref:
        "http://www.rwth-aachen.de/cms/root/Footer/Services/~cesv/Disclaimer/",
      imprint: "Impressum",
      contact: "Kontakt",
    },
    profile: "Profil",
    preferences: "Einstellungen",
    search: "Suche",
    langBox: "En",
    bannerText:
      'Coscine befindet sich derzeit in der Pilotphase. Sie können das System gerne testen, es kann aber noch zu Einschränkungen bei der Verfügbarkeit und Nutzung kommen! Feedback und Verbesserungsvorschläge können Sie gerne an [ <a href="mailto:servicedesk@rwth-aachen.de?subject=Coscine%20Pilot%20Program">Servicedesk</a> ] senden.',
    title: {
      project: {
        "CreateProject.aspx": "Projekt erstellen - {ProjectName} - Coscine",
        "CreateResource.aspx": "Ressource erstellen - {ProjectName} - Coscine",
        "EditProject.aspx": "Projekt bearbeiten - {ProjectName} - Coscine",
        "Home.aspx": "{ProjectName} - Coscine",
        "QuotaManagement.aspx": "Quota verwalten - {ProjectName} - Coscine",
        "UserManagement.aspx": "Mitglieder verwalten - {ProjectName} - Coscine",
        forms: {
          "Shared%20Documents":
            "Dokumentenbibliothek - {ProjectName} - Coscine",
        },
        lists: {
          "Announcement%20Board": "Ankündigungen - {ProjectName} - Coscine",
          "Discussion%20Board": "Diskussionsforum - {ProjectName} - Coscine",
        },
        resource: {
          "CreateResource.aspx":
            "Ressource bearbeiten - {ResourceName} - {ProjectName} - Coscine",
          "ViewResource.aspx":
            "Ressource anzeigen - {ResourceName} - {ProjectName} - Coscine",
        },
      },
      "CreateProject.aspx": "Projekt erstellen - Coscine",
      "Home.aspx": "Home - Coscine",
      "Search.aspx": "Suche - Coscine",
      "UserProfile.aspx": "Benutzerprofil - Coscine",
    },
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
