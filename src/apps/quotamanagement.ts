import Default from "./default";
import ResourceTypeDefinitions from "./resourcetypedefinitions";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    headline: "Quota Management",
    resources: "Resources",
    rangeText1: " GB of ",
    rangeText2: " GB reserved",
    gb: " GB ",

    projectLabel: "Project:",
    resourceTypeLabel: "Resource Type:",

    emptyProjectSelect: "-- Please select a project --",
    emptyResourceTypeSelect: "-- Please select a resource type --",
    noResourceTypeChoosen: "No resource type selected.",

    moreHelp: "Request more",
    moreHelpLink:
      "https://help.itc.rwth-aachen.de/en/service/b2b7729fd93f4c7080b475776f6b5d87/article/5404cfccf9e94f2abdb7d2fa36e722ab/",

    resource: "Resource",
    allocated: "Reserved",
    used: "Used",
    adjustQuota: "Adjust Quota",
    loading: " Loading...",

    emptyTableText: "No resources exist for this resource type.",

    connectionErrorTitle: "Resource quota retrieval not successfull",
    connectionErrorBody:
      "An error occured while retrieving the resource type quota. Please try again. If the error persists, contact us at servicedesk@itc.rwth-aachen.de",

    resourceTypeQuotaChangedSuccessTitle: "Quota extended successfully",
    resourceTypeQuotaChangedSuccessBody:
      "The quota for project {ProjectName} was successfully extended to {AmountInGB} GB.",
    resourceTypeQuotaChangedFailureTitle:
      "Resource quota extension not successfull",
    resourceTypeQuotaChangedFailureBody:
      "An error occured while extending the resource type quota. Please try again. If the error persists, contact us at servicedesk@itc.rwth-aachen.de",

    resourceQuotaChangedSuccessTitle: "Quota extended successfully",
    resourceQuotaChangedSuccessBody:
      "The quota for resource {ResourceName} was successfully extended to {AmountInGB} GB.",
    resourceQuotaChangedFailureTitle: "Quota extension not successfull",
    resourceQuotaChangedFailureBody:
      "An error occured while extending the quota. Please try again. If the error persists, contact us at servicedesk@itc.rwth-aachen.de",
  },
  de: {
    headline: "Quota Management",
    resources: "Ressourcen",
    rangeText1: " GB von ",
    rangeText2: " GB reserviert",
    gb: " GB ",

    projectLabel: "Projekt:",
    resourceTypeLabel: "Ressource Typ:",

    emptyProjectSelect: "-- Bitte wählen Sie ein Projekt --",
    emptyResourceTypeSelect: "-- Bitte wählen Sie einen Ressourcen Typ --",
    noResourceTypeChoosen: "Es wurde kein Ressourcen Typ ausgewählt.",

    moreHelp: "Mehr anfordern",
    moreHelpLink:
      "https://help.itc.rwth-aachen.de/service/b2b7729fd93f4c7080b475776f6b5d87/article/5404cfccf9e94f2abdb7d2fa36e722ab/",

    resource: "Ressource",
    allocated: "Reserviert",
    used: "Belegt",
    adjustQuota: "Quota anpassen",

    emptyTableText: "Für diesen Ressourcentyp existieren keine Ressourcen.",

    connectionErrorTitle: "Fehler bei der Abfrage der Quotas",
    connectionErrorBody:
      "Bei der Abfrage der Quotas ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Wenn der Fehler weiter auftritt, wenden Sie sich bitte an servicedesk@rwth-aachen.de",

    resourceTypeQuotaChangedSuccessTitle: "Quota erfolgreich verändert",
    resourceTypeQuotaChangedSuccessBody:
      "Die Quota im Projekt {ProjectName} wurde auf {AmountInGB} GB gesetzt.",
    resourceTypeQuotaChangedFailureTitle: "Fehler beim Ändern der Quota",
    resourceTypeQuotaChangedFailureBody:
      "Beim Ändern der Quota ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Wenn der Fehler weiter auftritt, wenden Sie sich bitte an servicedesk@rwth-aachen.de",

    resourceQuotaChangedSuccessTitle: "Quota erfolgreich verändert",
    resourceQuotaChangedSuccessBody:
      "Die Quota in Ressource {ResourceName} wurde auf {AmountInGB} GB gesetzt.",
    resourceQuotaChangedFailureTitle: "Fehler beim Ändern der Quota",
    resourceQuotaChangedFailureBody:
      "Beim Ändern der Quota ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Wenn der Fehler weiter auftritt, wenden Sie sich bitte an servicedesk@rwth-aachen.de",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge(MergeUtil.merge({}, ResourceTypeDefinitions), Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
