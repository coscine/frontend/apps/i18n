import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    home: "Home",

    announcements: "Announcements",
    documents: "Documents",
    discussions: "Discussion",

    project: "Project: ",
    noProjects: "No projects",

    projects: "Projects",
    resources: "Resources",
    subprojects: "Sub-Projects",
    settings: "Settings",

    projectEdit: "Edit Project",
    userManagement: "Manage Members",
    quotaManagement: "Manage Quota",
  },
  de: {
    home: "Übersicht",

    announcements: "Ankündigungen",
    documents: "Dokumente",
    discussions: "Diskussionen",

    project: "Projekt: ",
    noProjects: "Keine Projekte",

    projects: "Projekte",
    resources: "Ressourcen",
    subprojects: "Unterprojekte",
    settings: "Einstellungen",

    projectEdit: "Projekt bearbeiten",
    userManagement: "Mitglieder verwalten",
    quotaManagement: "Quota verwalten",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
