import Default from "./default";
import ResourceTypeDefinitions from "./resourcetypedefinitions";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    popoverLicense: "For more information on licenses see ",
    popoverUsageRights: "For more information on internal rules for reuse see ",
    help: "Help",

    resourceCreationErrorHeader: "Failed to create resource",
    resourceCreationError:
      "Please try again. If the problem persists, please contact ",
    confirm: "Confirm",

    projectLabel: "Project:",
    resourceLabel: "Resource:",

    emptyProjectSelect: "-- Please select a project --",
    emptyResourceSelect: "-- Please select a resource --",
    noResourceChoosen: "No resource selected.",
    savingResourceSuccessTitle: "Save Resource",
    savingResourceSuccessBody: "Resource was saved successfully.",
    deletingResourceSuccessTitle: "Delete Resource",
    deletingResourceSuccessBody: "Resource was deleted successfully.",

    savingResourceFailureTitle: "Save Resource",
    savingResourceFailureBody: "An error occurred while saving the resource.",
    deletingResourceFailureTitle: "Delete Resource",
    deletingResourceFailureBody:
      "An error occurred while deleting the resource.",

    displayName: "Display Name:",
    persistentId: "Persistent Id:",
    type: "Type:",
    quota: "Quota:",

    pid: "Persistent Identifier (PID)",
    pidLabel: "Persistent Identifier (PID):",
    pidHelp: "This is the Persistent Identifier (PID) of the resource.",
    pidToClipboard: "PID has been copied to clipboard.",

    needMore: "Need more?",

    title: "Edit Resource",

    resourceconfiguration: "Resource Configuration",
    generalinformation: "General Information",

    resourceTypesLabel: "Resource Type:",
    resourceTypes: "Resource Type",
    selectResourceType: "Please select a resource type",

    BucketName: "Bucket Name",
    BucketNameLabel: "Bucket Name:",
    BucketNameHelp:
      "This is a required field and can only be from 3 - 63 characters long.",

    Size: "Resource Size",
    SizeLabel: "Resource Size:",
    SizeHelp: "This is a required field.",

    AccessKey: "Access Key",
    AccessKeyLabel: "Access Key:",
    AccessKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    ReadAccessKey: "Access Key (Reading)",
    ReadAccessKeyLabel: "Access Key (Reading):",
    ReadAccessKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    WriteAccessKey: "Access Key (Writing)",
    WriteAccessKeyLabel: "Access Key (Writing):",
    WriteAccessKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    SecretKey: "Secret Key",
    SecretKeyLabel: "Secret Key:",
    SecretKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    ReadSecretKey: "Secret Key (Reading)",
    ReadSecretKeyLabel: "Secret Key (Reading):",
    ReadSecretKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    WriteSecretKey: "Secret Key (Writing)",
    WriteSecretKeyLabel: "Secret Key (Writing):",
    WriteSecretKeyHelp:
      "This is a required field and can only be up to 200 characters long.",

    Endpoint: "Entry Point",
    EndpointLabel: "Entry Point:",
    EndpointHelp:
      "This is a required field and can only be up to 200 characters long.",

    ResourceUrl: "Resource Url",
    ResourceUrlLabel: "Resource Url:",
    ResourceUrlHelp:
      "This is a required field and can only be up to 200 characters long.",

    RepositoryNumber: "Repository Id",
    RepositoryNumberLabel: "Repository Id:",
    RepositoryNumberHelp: "This is a required field and can only be an number.",

    RepositoryUrl: "Repository Url",
    RepositoryUrlLabel: "Repository Url:",
    RepositoryUrlHelp:
      "This is a required field and can only be up to 500 characters long.",

    Token: "Token",
    TokenLabel: "Token:",
    TokenHelp:
      "This is a required field and can only be up to 100 characters long.",

    ResourceName: "Resource Name",
    ResourceNameLabel: "Resource Name:",
    ResourceNameHelp:
      "This is a required field and can only be up to 200 characters long.",

    DisplayName: "Display Name",
    DisplayNameLabel: "Display Name:",
    DisplayNameHelp:
      "This is a required field and can only be up to 25 characters long.",

    ResourceDescription: "Resource Description",
    ResourceDescriptionLabel: "Resource Description:",
    ResourceDescriptionHelp:
      "This is a required field and can only be up to 5000 characters long.",

    ResourceKeywords: 'Type then press "enter" to insert a Keyword.',
    ResourceKeywordsLabel: "Resource Keywords:",
    ResourceKeywordsHelp:
      "This is a required field and can only be up to 1000 characters.",

    ResourceDiscipline: "Discipline",
    ResourceDisciplineLabel: "Discipline:",

    ResourceVisibility: "Visibility",
    ResourceVisibilityLabel: "Visibility:",

    ResourceLicense: "License",
    ResourceLicenseLabel: "License:",
    SelectResourceLicense: "Please select a License",

    ResourceUsageRights: "Usage Rights",
    ResourceUsageRightsLabel: "Internal Rules for Reuse:",
    ResourceUsageRightsHelp:
      "This is a required field and can only be up to 500 characters long.",

    TagPlaceholder: "You can add this tag",

    metadata: "Resource Metadata",

    applicationProfilesLabel: "Application Profiles:",
    applicationProfiles: "Application Profiles",

    selectApplicationProfile: "Please select an application profile",

    CreatorLabel: "Creator:",
    Creator: "Creator",
    TitleLabel: "Title:",
    Title: "Title",
    ProductionDateLabel: "Production Date:",
    ProductionDate: "Production Date",
    SubjectAreaLabel: "Subject Area:",
    SubjectArea: "Subject Area",
    ResourceLabel: "Resource:",
    Resource: "Resource",
    RightsLabel: "Rights:",
    Rights: "Rights",
    RightsholderLabel: "Rightsholder:",
    Rightsholder: "Rightsholder",

    DeleteButton: "Delete",
    cancel: "Cancel",
    DeleteModalHeadline: "Do you really want to delete this resource?",
    DeleteModalDescription:
      "If you are sure you really want to delete this resource, please repeat the resource name:",
    DeleteModalHelp: "The entered name does not match the resource name.",

    tabSetup: "Setup",
    tabGeneralInfo: "General Information",
    tabMetadata: "Resource Metadata",

    tabActions: "Actions",
    ArchiveLabel: "Archive:",
    DeleteResourceLabel: "Delete Resource:",

    ArchivedModalTitle: "Archive resource",
    ArchivedModalHeader:
      "If the status of a resource is set to archived, data and metadata can not be changed by users any longer. However, reading the data and downloading files is still possible. <br> The status can be unset by project owners if the data has to be extended or updated.",
    Archive: "Archive",
    unArchivedModalTitle: "Unset archived status",
    unArchivedModalHeader:
      "If the archived-status is unset, resources return to normal functionality (e.g. adding new files and updating metadata). <br> The archived status can be set again by project owners to prevent users from editing data.",
    unArchive: "Set to normal",

    ArchivedToastHeader: "Resource archived",
    ArchivedToastBody: "The resource was set to archived successfully.",
    unArchivedToastHeader: "Resource status reset",
    unArchivedToastBody:
      "The resource status was reset to normal successfully.",

    PID: "Persistent ID:",
  },
  de: {
    popoverLicense: "Für weitere Informationen zu Lizenzen siehe ",
    popoverUsageRights: "Für weitere Informationen zu internen Regeln zur Nachnutzung siehe ",
    help: "Hilfe",

    resourceCreationErrorHeader: "Fehler beim Erstellen der Ressource",
    resourceCreationError:
      "Bitte versuchen Sie es erneut. Falls der Fehler wieder Auftritt, wenden Sie sich bitte an ",
    confirm: "Bestätigen",

    projectLabel: "Projekt:",
    resourceLabel: "Ressource:",

    emptyProjectSelect: "-- Bitte wählen Sie ein Projekt --",
    emptyResourceSelect: "-- Bitte wählen Sie eine Ressource --",
    noResourceChoosen: "Es wurde keine Ressource ausgewählt.",
    savingResourceSuccessTitle: "Ressource Speichern",
    savingResourceSuccessBody: "Ressource wurde erfolgreich gespeichert.",
    deletingResourceSuccessTitle: "Ressource löschen",
    deletingResourceSuccessBody: "Ressource wurde erfolgreich gelöscht.",

    savingResourceFailureTitle: "Save Resource",
    savingResourceFailureBody:
      "Beim Speichern der Ressource ist ein Fehler aufgetretten.",
    deletingResourceFailureTitle: "Delete Resource",
    deletingResourceFailureBody:
      "Beim Löschen der Ressource ist ein Fehler aufgetretten.",

    displayName: "Anzeigename:",
    persistentId: "Persistente Id:",
    type: "Typ:",
    quota: "Quota:",

    pid: "Persistent Identifier (PID)",
    pidLabel: "Persistent Identifier (PID):",
    pidHelp: "Dies ist die PID einer Ressource.",
    pidToClipboard: "PID in Zwischenablage kopiert.",

    needMore: "Benötigen Sie mehr Speicher?",

    title: "Ressource editieren",

    resourceconfiguration: "Ressourcen Konfiguration",
    generalinformation: "Generelle Informationen",

    resourceTypesLabel: "Ressourcentypen:",
    resourceTypes: "Ressourcentyp",
    selectResourceType: "Bitte wählen Sie einen Ressourcentypen aus",

    BucketName: "Ressourcenname",
    BucketNameLabel: "Ressourcenname:",
    BucketNameHelp:
      "Dieses Feld ist erforderlich und besitzt eine Minimallänge von 3 und eine Maximallänge von 63 Zeichen.",

    Size: "Speicher für Ressource",
    SizeLabel: "Speicher für Ressource:",
    SizeHelp: "Dieses Feld ist erforderlich.",

    AccessKey: "Access Key",
    AccessKeyLabel: "Access Key:",
    AccessKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    ReadAccessKey: "Access Key (Lesen)",
    ReadAccessKeyLabel: "Access Key (Lesen):",
    ReadAccessKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    WriteAccessKey: "Access Key (Schreiben)",
    WriteAccessKeyLabel: "Access Key (Schreiben):",
    WriteAccessKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    SecretKey: "Secret Key",
    SecretKeyLabel: "Secret Key:",
    SecretKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    ReadSecretKey: "Secret Key (Lesen)",
    ReadSecretKeyLabel: "Secret Key (Lesen):",
    ReadSecretKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    WriteSecretKey: "Secret Key (Schreiben)",
    WriteSecretKeyLabel: "Secret Key (Schreiben):",
    WriteSecretKeyHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    Endpoint: "Ressourcen Url",
    EndpointLabel: "Ressourcen Url:",
    EndpointHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    ResourceUrl: "Ressourcen Url",
    ResourceUrlLabel: "Ressourcen Url:",
    ResourceUrlHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    RepositoryNumber: "Repository Id",
    RepositoryNumberLabel: "Repository Id:",
    RepositoryNumberHelp:
      "Dieses Feld ist erforderlich und kann nur eine Nummer sein.",

    RepositoryUrl: "Repository Url",
    RepositoryUrlLabel: "Repository Url:",
    RepositoryUrlHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 500 Zeichen.",

    Token: "Token",
    TokenLabel: "Token:",
    TokenHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 100 Zeichen.",

    ResourceName: "Ressourcenname",
    ResourceNameLabel: "Ressourcenname:",
    ResourceNameHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 200 Zeichen.",

    DisplayName: "Anzeigename",
    DisplayNameLabel: "Anzeigename:",
    DisplayNameHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 25 Zeichen.",

    ResourceDescription: "Ressourcenbeschreibung",
    ResourceDescriptionLabel: "Ressourcenbeschreibung:",
    ResourceDescriptionHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 5000 Zeichen.",

    ResourceKeywords:
      'Tippen Sie und drücken dann "Enter" um ein Schlagwort einzufügen.',
    ResourceKeywordsLabel: "Ressourcenschlagwörter:",
    ResourceKeywordsHelp:
      "Dieses Feld ist optional und besitzt eine Maximallänge von 1000 Zeichen.",

    ResourceDiscipline: "Disziplin",
    ResourceDisciplineLabel: "Disziplin:",

    ResourceVisibility: "Sichtbarkeit",
    ResourceVisibilityLabel: "Sichtbarkeit:",

    ResourceLicense: "Lizenz",
    ResourceLicenseLabel: "Lizenz:",
    SelectResourceLicense: "Bitte wählen Sie eine Lizenz aus",

    ResourceUsageRights: "Verwendungsrechte",
    ResourceUsageRightsLabel: "Interne Regeln zur Nachnutzung:",
    ResourceUsageRightsHelp:
      "Dieses Feld ist erforderlich und besitzt eine Maximallänge von 500 Zeichen.",

    TagPlaceholder: "Sie können diesen Tag hinzufügen",

    metadata: "Metadaten der Ressource",

    applicationProfilesLabel: "Applikationsprofile:",
    applicationProfiles: "Applikationsprofile",

    selectApplicationProfile: "Bitte wählen sie ein Applikationsprofil aus",

    CreatorLabel: "Ersteller:",
    Creator: "Ersteller",
    TitleLabel: "Titel:",
    Title: "Titel",
    ProductionDateLabel: "Erstelldatum:",
    ProductionDate: "Erstelldatum",
    SubjectAreaLabel: "Fachrichtung:",
    SubjectArea: "Fachrichtung",
    ResourceLabel: "Ressource:",
    Resource: "Ressource",
    RightsLabel: "Berechtigung:",
    Rights: "Berechtigung",
    RightsholderLabel: "Rechteinhaber:",
    Rightsholder: "Rechteinhaber",

    DeleteButton: "Löschen",
    cancel: "Abbrechen",
    DeleteModalHeadline: "Ressource wirklich entfernen?",
    DeleteModalDescription:
      "Wenn Sie sicher sind, dass Sie diese Ressource entfernen möchten, wiederholen Sie bitte den Ressourcennamen:",
    DeleteModalHelp:
      "Der angegebene Name stimmt nicht mit dem Ressourcenname überein",

    tabSetup: "Setup",
    tabGeneralInfo: "Generelle Informationen",
    tabMetadata: "Metadaten der Ressource",

    tabActions: "Aktionen",
    ArchiveLabel: "Archiv:",
    DeleteResourceLabel: "Ressource Löschen:",

    ArchivedModalTitle: "Ressource archivieren",
    ArchivedModalHeader:
      "Wenn der Status einer Ressource auf archiviert gesetzt wird, können Daten und Metadaten von Benutzern nicht mehr geändert werden. Das Lesen der Daten und das Herunterladen von Dateien ist jedoch weiterhin möglich. <br> Der Status kann von Projektbesitzern zurückgesetzt werden, wenn die Daten ergänzt oder aktualisiert werden sollen.",
    Archive: "Archivieren",
    unArchivedModalTitle: "Aufheben des archivierten Status",
    unArchivedModalHeader:
      "Wenn der archivierte Status nicht mehr gesetzt ist, kehren die Ressourcen zur normalen Funktionalität zurück (z. B. Hinzufügen neuer Dateien und Aktualisieren von Metadaten).<br> Der Status kann von Projektbesitzern wieder gesetzt werden, um zu verhindern, dass Benutzer Daten bearbeiten.",
    unArchive: "Status entfernen",

    ArchivedToastHeader: "Ressource archiviert",
    ArchivedToastBody: " Die Ressource wurde erfolgreich archiviert.",
    unArchivedToastHeader: "Ressourcenstatus zurückgesetzt",
    unArchivedToastBody:
      "Der Status der Ressource wurde erfolgreich zurückgesetzt.",

    PID: "Persistente ID:",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge(MergeUtil.merge({}, ResourceTypeDefinitions), Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
