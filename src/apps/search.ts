import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    project: "Projects:",
    subproject: "Sub-Projects:",
    resource: "Resources:",
    file: "Files:",
    search: "Search",
    searchIn: "Search in ",
    noHits: "No Result Found.",
    editResource: "edit",
    viewResource: "view",
  },
  de: {
    project: "Projekte:",
    subproject: "Unterprojekte:",
    resource: "Ressourcen:",
    file: "Dateien:",
    search: "Suchen",
    searchIn: "Suchen in ",
    noHits: "Die Suche ergab keine Treffer.",
    editResource: "bearbeiten",
    viewResource: "anzeigen",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
