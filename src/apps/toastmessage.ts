import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    title: "Your session expired",
    message: "To continue working, log in again:",
    link: "Login",
  },
  de: {
    title: "Ihre Sitzung ist abgelaufen",
    message: "Um weiterzuarbeiten, loggen Sie sich erneut ein:",
    link: "Login",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
