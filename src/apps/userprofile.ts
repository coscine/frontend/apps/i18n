import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    userProfile: "User profile",
    userListHeadline: "Personal Information",
    title: "Select Title",
    givenname: "First Name",
    surname: "Last Name",
    email: "Email",
    organization: "Organization",
    institute: "Institute",
    discipline: "Select Discipline(s)",
    userpreferences: "User Preferences",
    connectedaccounts: "Connected Accounts",

    titleLabel: "Title:",
    givennameLabel: "First Name:",
    surnameLabel: "Last Name:",
    emailLabel: "Email:",
    organizationLabel: "Organization:",
    instituteLabel: "Institute:",
    disciplineLabel: "Discipline:",
    languageLabel: "Language:",
    orcidLabel: "ORCID:",
    shibbolethLabel: "Single Sign-On:",
    connect: "connect",
    connected: "connected",
    save: "save",

    MultiselectPlaceholderTitle: "Please select a title.",
    MultiselectSelectEnter: "Press enter to select.",
    MultiselectNoResults:
      "No elements found. Consider changing the search query.",
    MultiselectNoOptions: "List is empty.",
    MultiselectNoOptionsOrganization: "Search for organizations",

    emailChangedTitle: "Email address changed",
    emailChangedMessage:
      "Email address was changed. A mail for confirmation was send to the new email address.",

    emailNoAddress: "No email address given.",
    emailPendingConfirmation: "Email confirmation pending.",

    // -- TokenUI
    AccessTokenHeader: "Access Token:",
    AccessTokenBodytext:
      "You can generate a personal access token for each application you use that needs access to the Coscine API. See the documentation for further information.",
    TokenName: "Token Name",
    TokenNameLabel: "Token Name:",
    TokenExpireLabel: "Expires at:",
    TokenExpire: "Expiration Date",
    TokenCreateBtn: "Create Access Token",
    TokenTableName: "Token Name",
    TokenTableCreated: "Created",
    TokenTableExpires: "Expires",
    TokenTableAction: "Action",
    TokenRevokeBtn: "revoke",
    emptyTableText: "You have no active access token.",
    // -- Modal contents for revoking access-token
    RevokeModalTitle: "Revoke Token",
    RevokeModalText:
      "Are you sure you want to revoke this token? This action can not be undone.",
    // -- Modal contents for creating access-token
    CreateModalTitle: "Your new personal access token",
    CreateModalText:
      "Make sure you save it - you won't be able to access it again.",
    CreateModalCloseBtn: "close",
    CopyToClipboard: "Token has been copied to clipboard",
  },
  de: {
    userProfile: "Nutzerprofil",
    userListHeadline: "Persönliche Daten",
    title: "Titel auswählen",
    givenname: "Vorname",
    surname: "Name",
    email: "E-Mail",
    organization: "Organisation",
    institute: "Institut",
    discipline: "Disziplin",
    userpreferences: "Benutzer Voreinstellungen",
    connectedaccounts: "Verbundene Accounts",

    titleLabel: "Titel:",
    givennameLabel: "Vorname:",
    surnameLabel: "Name:",
    emailLabel: "E-Mail:",
    organizationLabel: "Organisation:",
    instituteLabel: "Institut:",
    disciplineLabel: "Disziplin:",
    languageLabel: "Sprache:",
    orcidLabel: "ORCID:",
    shibbolethLabel: "Single Sign-On:",
    connect: "verbinden",
    connected: "verbunden",
    save: "save",

    MultiselectPlaceholderTitle: "Bitte wählen Sie einen Titel.",
    MultiselectSelectEnter: "Zum Auswählen Enter drücken.",
    MultiselectNoResults:
      "Keine Ergebnisse verfügbar. Bitte passen Sie die Suchanfrage an.",
    MultiselectNoOptions: "Die Liste ist leer.",
    MultiselectNoOptionsOrganization: "Suche nach Organisationen",

    emailChangedTitle: "Änderung der E-Mail-Adresse",
    emailChangedMessage:
      "E-Mail-Adresse wurde geändert. Eine E-Mail zur Bestätigung der Änderung wurde an die neue E-Mail-Adresse gesendet.",

    emailNoAddress: "Keine E-Mail-Adresse angegeben.",
    emailPendingConfirmation: "E-Mail-Bestätigung ausstehend.",

    // -- TokenUI
    AccessTokenHeader: "Zugriffstoken erstellen:",
    AccessTokenBodytext:
      "Sie können für jede von Ihnen verwendete Anwendung, die Zugriff auf die Coscine-API benötigt, ein persönliches Zugriffstoken generieren. Weitere Informationen finden Sie in der Dokumentation.",
    TokenName: "Token Name",
    TokenNameLabel: "Token Name:",
    TokenExpireLabel: "Verfällt am:",
    TokenExpire: "Verfallsdatum",
    TokenCreateBtn: "Zugriffstoken erstellen",
    TokenTableName: "Token Name",
    TokenTableCreated: "Erstellt",
    TokenTableExpires: "Verfällt",
    TokenTableAction: "Aktion",
    TokenRevokeBtn: "widerrufen",
    emptyTableText: "Sie haben kein aktives Zugriffs Token.",
    // -- Modal contents for revoking access-token
    RevokeModalTitle: "Token Widerrufen",
    RevokeModalText:
      "Sind Sie sicher, dass Sie diese Token widerrufen wollen? Diese Aktion kann nicht rückgängig gemacht werden.",
    // -- Modal contents for creating access-token
    CreateModalTitle: "Ihr neues persönliches Access Token",
    CreateModalText:
      "Stellen Sie sicher, dass Sie es speichern - Sie können dann nicht mehr darauf zugreifen.",
    CreateModalCloseBtn: "schließen",
    CopyToClipboard: "Token wurde in die Zwischenablage kopiert",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
