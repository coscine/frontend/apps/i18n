export default {
  en: {
    buttons: {
      save: "Save",
      cancel: "Cancel",
      back: "Back",
      delete: "Delete",
    },
    toastMessages: {
      saveFailedTitle: "Error on saving",
      saveFailedMessage:
        "An error occured. Please try again. If the error persists, please contact servicedesk@rwth-aachen.de.",
      saveSuccessfulTitle: "Saved successfully",
      saveSuccessfulMessage: "The data has been saved successfully.",
    },
    email: {
      serviceDeskName: "Servicedesk",
      serviceDeskMailTo:
        "mailto:servicedesk@itc.rwth-aachen.de?subject=CoScInE%20Pilot%20Program",
    },
    banner: {
      pilot: {
        pilotBannerText1: `Coscine is currently in the first pilot phase. So you are welcome to
                 log in and explore the system, but you may experience technical difficulties
                 due to active developments. Please contact us if you want to be part of the
                 pilot program. If you have feedback you are welcome to send it to [`,
        pilotBannerText2: "]",
      },
      maintenance: {
        notificationDefaultText:
          "Currently, you may experience restrictions when using Coscine. Details can be found at [",
        notificationDeployment:
          "Due to ongoing updates, you may experience restrictions when using Coscine.",
        notificationMalfunction:
          "Due to a malfunction Coscine is currently unavailable. Details can be found at [",
        notificationPartiaMulfunction:
          "Due to a partial malfunction Coscine is currently unavailable. Details can be found at [",
        notificationInteruption:
          "Due to a interuption of a service Coscine is currently unavailable. Details can be found at [",
        notificationLimitedOperability:
          "Coscine is currently offering limited operability. Details can be found at [",
        notificationMaintenance:
          "Coscine is currently undergoing maintenance. You may experience restrictions in its use. Details can be found at [",
        notificationPartialMaintenance:
          "Coscine is currently undergoing maintenance. You may experience restrictions in its use. Details can be found at [",
        notificationEndText: "].",
        moreInformation: "more information",
      },
    },
    archive: {
      archived: "Archived",
    },
  },
  de: {
    buttons: {
      save: "Speichern",
      cancel: "Abbrechen",
      back: "Zurück",
    },
    toastMessages: {
      saveFailedTitle: "Fehler beim Speichern",
      saveFailedMessage:
        "Es ist ein Fehler ausgetreten. Bitte versuchen Sie es erneut. Falls der Fehler weiterhin auftritt, wenden Sie sich bitte an servicedesk@rwth-aachen.de.",
      saveSuccessfulTitle: "Änderungen erfolgreich",
      saveSuccessfulMessage: "Die Daten wurden erfolgreich gespeichert.",
    },
    email: {
      serviceDeskName: "Servicedesk",
      serviceDeskMailTo:
        "mailto:servicedesk@itc.rwth-aachen.de?subject=CoScInE%20Pilot%20Program",
    },
    banner: {
      pilot: {
        pilotBannerText1: `Coscine befindet sich derzeit in der ersten Pilotphase. Sie können sich also gerne
                einloggen und das System erkunden, aber aufgrund aktiver Entwicklungen kann es zu 
                technischen Schwierigkeiten kommen. Bitte kontaktieren Sie uns, 
                wenn Sie an dem Pilotprogramm teilnehmen möchten. Feedback und
                Verbesserungsvorschläge können Sie gerne an [`,
        pilotBannerText2: "] senden.",
      },
      maintenance: {
        notificationDefaultText:
          "Derzeit kann es unter Umständen zu Einschränkungen bei der Nutzung von Coscine kommen. Details finden Sie unter [",
        notificationDeployment:
          "Aufgrund einer Aktualisierung kann es zu Einschränkungen bei der Nutzung von Coscine kommen.",
        notificationMalfunction:
          "Durch eine Störung ist Coscine derzeit nicht verfügbar. Details finden Sie unter [",
        notificationPartiaMulfunction:
          "Durch eine Teilstörung ist Coscine derzeit nicht verfügbar. Details finden Sie unter [",
        notificationInteruption:
          "Durch die Unterbrechung eines Dienstes ist Coscine derzeit nicht verfügbar. Details finden Sie unter [",
        notificationLimitedOperability:
          "Coscine ist derzeit nur eingeschränkt verfügbar.  Details finden Sie unter [",
        notificationMaintenance:
          "Coscine wird derzeit gewartet. Es können Einschränkungen bei der Nutzung auftreten. Details finden Sie unter [",
        notificationPartialMaintenance:
          "Coscine wird derzeit gewartet. Es können Einschränkungen bei der Nutzung auftreten. Details finden Sie unter [",
        notificationEndText: "].",
        moreInformation: "weitere Informationen",
      },
    },
    archive: {
      archived: "Archiviert",
    },
  },
};
