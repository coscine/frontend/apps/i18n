import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    addResource: "Add Resource",
    resources: "Resources",
  },
  de: {
    addResource: "Ressource hinzufügen",
    resources: "Ressourcen",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
