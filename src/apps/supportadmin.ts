import Default from "./default";
import ResourceTypeDefinitions from "./resourcetypedefinitions";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    headline: "Quota Admin Panel:",
    projectInputTooltip: "Enter slug or GUID to select Project",
    projectSelectButton: "Select",
    projectFound: "Project found",
    projectNotSelected: "No project selected",
    projectNotFound: "Project not found",
    projectName: "Name",
    projectShortName: "Short name",
    projectGuid: "GUID",
    projectQuotaHeadline: "Quotas",
    resourceType: "Ressource Type",
    currentQuota: "Current Project Reserved Quota",
    allocatedQuota: "Total Resource Reserved Quota",
    newQuota: "New Project Quota",
    action: "Action",
    newQuotaInputPlaceHolder: "Enter in GB",
    save: "Save",
    successNotificationHeadline: "Quota changed successfully",
    successNotificationText:
      "{resourceType} quota for project {projectName} set to {newQuota} GB",
    failureNotificationHeadline: "Error",
    failureNotificationText: "Updating Quota failed.",
    queryProjectFailureHeadline: "Error",
    queryProjectFailureText: "An error occured looking for the project.",
  },
  de: {
    headline: "Quota Admin Panel:",
    projectInputPlaceHolder: "Projekt-GUID oder -slug eingeben",
    projectSelectButton: "Auswählen",
    projectFound: "Projekt gefunden",
    projectNotSelected: "Kein Projekt ausgewählt",
    projectNotFound: "Projekt nicht gefunden",
    projectName: "Name",
    projectShortName: "Kurzbezeichnung",
    projectGuid: "GUID",
    projectQuotaHeadline: "Quoten",
    resourceType: "Resourcentyp",
    currentQuota: "Aktuelles Projekt Reserviertes Quote",
    allocatedQuota: "Reservierte Gesamtquote für Ressourcen",
    newQuota: "Neue Projektquote",
    action: "Aktion",
    newQuotaInputPlaceHolder: "In GB angeben",
    save: "Speichern",
    successNotificationHeadline: "Quote erfolgreich geändert",
    successNotificationText:
      "{resourceType} Quote für Projekt {projectName} gesetzt auf {newQuota} GB",
    failureNotificationHeadline: "Fehler",
    failureNotificationText: "Aktualisierung der Quote fehlgeschlagen.",
    queryProjectFailureHeadline: "Fehler",
    queryProjectFailureText:
      "Beim Suchen nach dem Project ist ein Fehler aufgetreten.",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge(MergeUtil.merge({}, ResourceTypeDefinitions), Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
