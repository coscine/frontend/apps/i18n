import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    selectPlaceholder: "Select...",
    invisibilityInfo:
      "Fields can be hidden to remove non-applicable fields from the metadata view. Mandatory fields can only be hidden if they have a default value that is marked as unchangeable.",
  },
  de: {
    selectPlaceholder: "Auswählen...",
    invisibilityInfo:
      "Felder können ausgeblendet werden, um nicht zutreffende Felder aus der Metadatenansicht zu entfernen. Pflichtfelder können nur dann ausgeblendet werden, wenn sie einen Standardwert haben, der als nicht änderbar gekennzeichnet ist.",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
