import Default from "./default";
import { MergeUtil } from "@coscine/app-util";

const appLanguageStrings = {
  en: {
    addProject: "Add Project",
    headline: "Projects",
  },
  de: {
    addProject: "Projekt hinzufügen",
    headline: "Projekte",
  },
};

const languageStrings = MergeUtil.merge(
  MergeUtil.merge({}, Default),
  appLanguageStrings
);

export default {
  languageStrings,
};
