declare type CoscineType = {
  i18n: Record<string, VueI18n.LocaleMessages | undefined>;
};

declare const coscine: CoscineType;

declare interface Window {
  coscine: CoscineType;
}

declare const _spPageContextInfo: unknown;
