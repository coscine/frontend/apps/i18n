import Default from "./apps/default";
import FormGenerator from "./apps/form-generator";
import Login from "./apps/login";
import Project from "./apps/project";
import ProjectCreation from "./apps/project-creation";
import ProjectCreate from "./apps/projectcreate";
import ProjectEdit from "./apps/projectedit";
import ProjectView from "./apps/projectview";
import QuotaManagement from "./apps/quotamanagement";
import ResourceContentView from "./apps/resourcecontentview";
import ResourceCreation from "./apps/resourcecreation";
import ResourceEdit from "./apps/resourceedit";
import Resources from "./apps/resources";
import RWTHMaster from "./apps/rwthmaster";
import Search from "./apps/search";
import SidebarMenu from "./apps/sidebarmenu";
import SupportAdmin from "./apps/supportadmin";
import ToastMessage from "./apps/toastmessage";
import UserList from "./apps/userlist";
import UserManagement from "./apps/usermanagement";
import UserProfile from "./apps/userprofile";
import Banner from "./apps/banner";

window.coscine = {
  i18n: {
    default: Default,
    "form-generator": FormGenerator.languageStrings,
    login: Login.languageStrings,
    project: Project.languageStrings,
    "project-creation": ProjectCreation.languageStrings,
    projectcreate: ProjectCreate.languageStrings,
    projectedit: ProjectEdit.languageStrings,
    projectview: ProjectView.languageStrings,
    quotamanagement: QuotaManagement.languageStrings,
    resourcecontentview: ResourceContentView.languageStrings,
    resourcecreation: ResourceCreation.languageStrings,
    resourceedit: ResourceEdit.languageStrings,
    resources: Resources.languageStrings,
    rwthmaster: RWTHMaster.languageStrings,
    search: Search.languageStrings,
    sidebarmenu: SidebarMenu.languageStrings,
    supportadmin: SupportAdmin.languageStrings,
    toastmessage: ToastMessage.languageStrings,
    userlist: UserList.languageStrings,
    usermanagement: UserManagement.languageStrings,
    userprofile: UserProfile.languageStrings,
    banner: Banner.languageStrings,
  },
};
